%undefine __cmake_in_source_build

# when libmajor changes, whole cmake toolchain will break
# rebuild cmake with bundled jsoncpp at first
%define libmajor 26

%bcond_without compatlib
%if %{with compatlib}
%define compatlib_version 1.9.5
%define compatlib_libmajor 25
%endif

Name:           jsoncpp
Version:        1.9.6
Release:        1
Summary:        JSON C++ library
License:        Public Domain or MIT
URL:            https://github.com/open-source-parsers/jsoncpp
Source0:        https://github.com/open-source-parsers/jsoncpp/archive/refs/tags/%{version}.tar.gz
%if %{with compatlib}
Source1:        https://github.com/open-source-parsers/jsoncpp/archive/refs/tags/%{compatlib_version}.tar.gz
%endif
BuildRequires:  gcc-c++
BuildRequires:  cmake >= 3.8.0
BuildRequires:  python3-devel

%description
JsonCpp is a C++ library that allows manipulating JSON values,
including serialization and deserialization to and from strings.
It can also preserve existing comment in unserialization/serialization steps,
making it a convenient format to store user input files.

%package        devel
Summary:        Development files for jsoncpp
Requires:       %{name} = %{version}-%{release}

%description    devel
Document files contain library and head files for jsoncpp.

%package        help
Summary:        Document for jsoncpp
BuildRequires:  doxygen graphviz hardlink
BuildArch:      noarch
Provides:       jsoncpp-doc = %{version}-%{release}
Obsoletes:      jsoncpp-doc < %{version}-%{release}

%description    help
Help document for jsoncpp.

%prep
%autosetup -p1 -n %{name}-%{version}
%if %{with compatlib}
tar xf %{S:1}
%endif

%build
%cmake -DBUILD_STATIC_LIBS=OFF -DJSONCPP_WITH_WARNING_AS_ERROR=OFF \
       -DBUILD_OBJECT_LIBS:BOOL=OFF \
       -DJSONCPP_WITH_PKGCONFIG_SUPPORT=ON -DJSONCPP_WITH_CMAKE_PACKAGE=ON \
       -DJSONCPP_WITH_POST_BUILD_UNITTEST=OFF
%cmake_build

cp -p %{__cmake_builddir}/version .
%{__python3} doxybuild.py --with-dot --doxygen %{_bindir}/doxygen
rm -f version

%if %{with compatlib}
pushd %{name}-%{compatlib_version}
%cmake -DBUILD_STATIC_LIBS=OFF -DJSONCPP_WITH_WARNING_AS_ERROR=OFF \
       -DBUILD_OBJECT_LIBS:BOOL=OFF \
       -DJSONCPP_WITH_PKGCONFIG_SUPPORT=ON -DJSONCPP_WITH_CMAKE_PACKAGE=ON \
       -DJSONCPP_WITH_POST_BUILD_UNITTEST=OFF
%cmake_build
popd
%endif

%install
%cmake_install

%if %{with compatlib}
pushd %{name}-%{compatlib_version}/%{__cmake_builddir}/lib
install -m755 lib%{name}.so.%{compatlib_version} lib%{name}.so.%{compatlib_libmajor} %{buildroot}%{_libdir}/
popd
%endif

%check
%ctest -j1

%files
%license LICENSE
%{_libdir}/lib%{name}.so.%{libmajor}
%{_libdir}/lib%{name}.so.%{version}
%if %{with compatlib}
%{_libdir}/lib%{name}.so.%{compatlib_libmajor}
%{_libdir}/lib%{name}.so.%{compatlib_version}
%endif

%files devel
%{_libdir}/lib%{name}.so
%{_includedir}/json
%{_libdir}/cmake/*
%{_libdir}/pkgconfig/%{name}.pc

%files help
%doc dist/doxygen/*

%changelog
* Sun Nov 03 2024 Funda Wang <fundawang@yeah.net> - 1.9.6-1
- update to 1.9.6
- add switch building compat lib

* Thu Nov 16 2023 liubo <liubo1@xfusion.com> - 1.9.5-5
- Use default rather than hard-coded 8 for maximum aggregate member alignment

* Wed Oct 18 2023 liubo <liubo1@xfusion.com> - 1.9.5-4
- Parse large floats as infinity

* Mon Jan 10 2022 shixuantong <shixuantong@huawei.com> - 1.9.5-3
- Delete so files of lower versions

* Wed Jan 05 2022 shangyibin <shangyibin1@huawei.com> - 1.9.5-2
- copy the .so file of the old version.

* Wed Dec 22 2021 shangyibin <shangyibin1@huawei.com> - 1.9.5-1
- upgrade version to 1.9.5

* Wed Feb 3 2021 liudabo <liudabo1@huawei.com> - 1.9.4-1
- upgrade version to 1.9.4

* Sat Aug 08 2020 lingsheng<lingsheng@huawei.com> - 1.9.3-2
- Remove old version so

* Fri Jul 31 2020 wenzhanli<wenzhanli2@huawei.com> - 1.9.3-1
- Type:NA
- ID:NA
- SUG:NA
- DESC:update version 1.9.3

* Mon Nov 25 2019 zhujunhao <zhujunhao5@huawei.com> - 1.8.4-6
- Package init
